package com.cy.pj.sys.service;

import com.cy.pj.common.pojo.PageObject;
import com.cy.pj.sys.pojo.SysLog;

public interface SysLogService{
    /**
     * 基于条件查询用户行为日志并进行封装
     * @param username 查询条件
     * @param pageCurrent 当前页面值
     * @return 封装分页查询结果
     */
    PageObject<SysLog> findPageObjects(String username,
                                       Integer pageCurrent);
}

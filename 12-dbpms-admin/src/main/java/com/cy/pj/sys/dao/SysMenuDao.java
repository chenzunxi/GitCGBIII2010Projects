package com.cy.pj.sys.dao;

import com.cy.pj.common.pojo.Node;
import com.cy.pj.sys.pojo.SysMenu;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface SysMenuDao{
    @Select("select id,name,parentId from sys_menus")
    public List<Node> findZtreeMenuNodes();

    int updateObject(SysMenu entity);
    int insertObject(SysMenu entity);
    /**查询所有菜单信息*/
    List<SysMenu> findObjects();
}
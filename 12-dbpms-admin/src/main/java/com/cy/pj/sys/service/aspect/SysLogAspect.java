package com.cy.pj.common.aspect;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;

/**
 * @Aspect 注解描述的类型为切面对象类型,此切面中可以定义多个切入点和通知方法.
 */
@Slf4j
@Aspect
@Component
public class SysLogAspect {

    @Autowired
    private Sys
    /**
     * @Pointcut注解用于定义切入点
     * bean("spring容器中bean的名字")这个表达式为切入点表达式定义的一种语法,
     * 它描述的是某个bean或多个bean中所有方法的集合为切入点,这个形式的切入点
     * 表达式的缺陷是不能精确到具体方法的.
     */
   /* @Pointcut("@annotation(com.cy.pj.common.annotation.RequiredCache)")
    public void doLog(){}//此方法只负责承载切入点的定义*/
    @Pointcut("@annotation(com.cy.pj.common.annotation.RequiredLog)")
    public void doLog(){}//此方法只负责承载切入点的定义






    /**
     * @Around注解描述的方法,可以在切入点执行之前和之后进行业务拓展,
     * 在当前业务中,此方法为日志通知方法
     * @param joinPoint 连接点对象,此对象封装了要执行的切入点方法信息.
     * 可以通过连接点对象调用目标方法.
     * @return 目标方法的执行结果
     * @throws Throwable
     */
    @Around("doLog()")
    public Object doAround(ProceedingJoinPoint jp)throws Throwable{
        log.info("Start:{}",System.currentTimeMillis());
        try {
            Object result = jp.proceed();//执行目标方法(切点方法中的某个方法)
            log.info("After:{}",System.currentTimeMillis());
            return result;//目标业务方法的执行结果
        }catch(Throwable e){
            e.printStackTrace();
            log.error("Exception:{}",System.currentTimeMillis());
            throw e;
        }
    }


    public Object do(ProceedingJoinPoint jp){

        MethodSignature ms= (MethodSignature) jp.getSignature();
        Class<?>targetCls=jp.getTarget().getClass();
        try {
            Method targetMethod=
                    targetCls.getDeclaredMethod(ms.getName(),ms.getParameterTypes());
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }

    }









}

package com.cy.pj.sys.pojo;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class SysLog implements Serializable {
    private static final long serialVersionUID = 4805229781871486416L;
    private Integer id;
    private String ip;
    private String username;
    private String operation;
    private String method;
    private String params;
    private Integer time;
    private Date createdTime;
}

package com.cy.pj.sys.controller;

import com.cy.pj.common.pojo.JsonResult;
import com.cy.pj.common.pojo.PageObject;
import com.cy.pj.sys.pojo.SysUser;
import com.cy.pj.sys.service.SysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/user/")
public class SysUserController {
    @Autowired
    private SysUserService sysUserService;

    @RequestMapping("doUpdateObject")
    public JsonResult doUpdateObject(SysUser entity,Integer[] roleIds){
        sysUserService.updateObject(entity,roleIds);
        return new JsonResult("update ok");
    }

    @GetMapping("doFindById/{id}") //这里的url要与客户端一致
    public JsonResult doFindById(@PathVariable Integer id){
        return new JsonResult(sysUserService.findById(id));
    }

    @PostMapping("doSaveObject")
    public JsonResult doSaveObject(SysUser entity,Integer[] roleIds){
        sysUserService.saveObject(entity,roleIds);

        return new JsonResult("save ok");
    }

    @RequestMapping("doValidById/{id}/{valid}")
    public JsonResult doValidById(@PathVariable Integer id,
                                  @PathVariable Integer valid){
        sysUserService.validById(id,valid);
        return new JsonResult("update ok");
    }

    @GetMapping("doFindPageObjects")
    public JsonResult doFindPageObjects(String username,
                                        Integer pageCurrent){
        PageObject<SysUser> pageObject=
                sysUserService.findPageObjects(username,pageCurrent);
        return new JsonResult(pageObject);
    }
}

package com.cy.pj.sys.controller;

import com.cy.pj.common.pojo.JsonResult;
import com.cy.pj.sys.pojo.SysMenu;
import com.cy.pj.sys.service.SysMenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/menu/")
public class SysMenuController {
    @Autowired
    private SysMenuService sysMenuService;

    @GetMapping("doFindZtreeMenuNodes")
    public /*@ResponseBody*/ JsonResult doFindZtreeMenuNodes(){
        return new JsonResult(sysMenuService.findZtreeMenuNodes());
    }

    @PutMapping("/doUpdateObject")
    public JsonResult doUpdateObject(@RequestBody SysMenu entity){
        sysMenuService.updateObject(entity);
        return new JsonResult("update ok");
    }
    @PostMapping("/doSaveObject")
    public JsonResult doSaveObject(@RequestBody SysMenu entity){
        sysMenuService.saveObject(entity);
        return new JsonResult("save ok");
    }

    //http://localhost/menu/doFindObjects
    @GetMapping("doFindObjects")
    public JsonResult doFindObjects(){
        List<SysMenu> list= sysMenuService.findObjects();
        return new JsonResult(list);
    }
}

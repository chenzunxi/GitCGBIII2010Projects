package com.cy.pj.sys.service;

import com.cy.pj.common.pojo.Node;
import com.cy.pj.sys.pojo.SysMenu;

import java.util.List;

public interface SysMenuService {
    public List<Node> findZtreeMenuNodes();
    int saveObject(SysMenu entity);
    int updateObject(SysMenu entity);
    List<SysMenu> findObjects();
}

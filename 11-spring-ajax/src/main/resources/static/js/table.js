function doInit(tableId,columns){
    //获取table对象
    let tableObj=$("#"+tableId);
    //初始化table的列标题
    tableObj.append(doCreateTHead(columns));
    //初始化table中的tbody内容
    tableObj.append(doCreateTBody(tableObj));
}
function doCreateTHead(columns){
    //创建一个thead对象
    let thead=$("<thead></thead>");
    //创建一个tr对象
    let tr=$("<tr></tr>");
    //迭代columns构建th对象并追加到tr
    for(let i=0;i<columns.length;i++){
        let th=$("<th></th>");
        th.html( columns[i].title);
        tr.append(th);
    }
    //将tr追加到thead中
    thead.append(tr);
    return thead;
}
function doCreateTBody(tableObj){
    //查找table对象中th的个数
    let cols=tableObj.find("thead>tr>th").length;
    return `<tbody>
               <tr>
                <td colspan='${cols}'>Data is Loading ...</td>
               </tr>
            </tbody>`;
}
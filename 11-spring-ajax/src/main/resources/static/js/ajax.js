function ajax(jsonObj){//jsonObj对象格式有要求
    //1.create xhr
    let xhr=createXHR(jsonObj.callback);
    //2.create connection
    let requestUrl=jsonObj.requestUrl;
    let requestParams=jsonObj.requestParams;
    let requestType=jsonObj.requestMethod;
    let url=requestType=="GET"?`${requestUrl}/${requestParams}`:requestUrl;
    xhr.open(requestType,url,true);
    if(requestType=="POST"){
        xhr.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
    }
    //3.send request
    xhr.send(requestType=="POST"?requestParams:null);
}

function ajaxGet(url,params,callback){//封装ajax get 请求模板代码
    //1.create xhr and set onreadystate change
    let xhr=createXHR(callback);
    //2.open connection
    xhr.open("GET",`${url}/${params}`,true);
    //3.send request
    xhr.send();
}

function ajaxDelete(url,params,callback){//封装ajax get 请求模板代码
    //1.create xhr and set onreadystate change
    let xhr=createXHR(callback);
    //2.open connection
    xhr.open("delete",`${url}/${params}`,true);
    //3.send request
    xhr.send();
}
function ajaxPut(url,params,callback){//封装ajax get 请求模板代码
    //1.create xhr and set onreadystate change
    let xhr=createXHR(callback);
    //2.open connection
    xhr.open("put",url,true);
    xhr.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
    //3.send request
    xhr.send(params);
}
function ajaxPost(url,params,callback){//封装ajax get 请求模板代码
    //1.create xhr and set onreadystate change
    let xhr=createXHR(callback);
    //2.open connection
    xhr.open("POST",url,true);
    xhr.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
    //3.send request
    xhr.send(params);
}
//向服务端提交json格式字符串
function ajaxPostJSON(url,params,callback){//封装ajax get 请求模板代码
    //1.create xhr and set onreadystate change
    let xhr=createXHR(callback);
    //2.open connection
    xhr.open("POST",url,true);
    xhr.setRequestHeader("Content-Type","application/json");
    //3.send request
    xhr.send(JSON.stringify(params));//将json对象转换为json格式字符串提交到服务端
}

function createXHR(callback){
    //1.create XHR object
    let xhr=new XMLHttpRequest();
    //2.set onreadystatechange
    xhr.onreadystatechange=function(){
        if(xhr.readyState==4){
            if(xhr.status==200){
                callback(xhr.responseText);
            }
        }
    }
    return xhr;
}